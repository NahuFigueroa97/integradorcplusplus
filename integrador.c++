#include <iostream>
#include <string>
#include <sstream>
#include <list>
using namespace std;
#define humLow 33
#define humMedium 65
#define humHigh 100
#define nDatos 3
#define valueMax 1000

class ViewData{
    public:
        ViewData();
        virtual void print(string, string, string, list <string>) = 0;
};

class ViewDataOne : public ViewData{
    public:
        ViewDataOne();
        void print(string, string, string, list <string>);
};

class ViewDataTwo : public ViewData{
    public:
        ViewDataTwo();
        void print(string, string, string, list <string>);
};

class ViewDataThree : public ViewData{
    public:
        ViewDataThree();
        void print(string, string, string, list <string> );
};

ViewData::ViewData(){}

void ViewData::print(string _temperature, string _humidity, string _pressure, list <string> _data){}

ViewDataOne::ViewDataOne(){}

void ViewDataOne::print(string _temperature, string _humidity, string _pressure, list <string> _data){
    cout << "1.Current conditions: " << _temperature << "C degrees and " << _humidity << "% humidity" << endl;
}

ViewDataTwo::ViewDataTwo(){}

void ViewDataTwo::print(string _temperature, string _humidity, string _pressure, list <string> _data){
    list <string>::iterator i;
    i = _data.begin();
    float max = 0, min = valueMax, avg = 0;
    while(i != _data.end()){
        if (stof(*i) > max){
        max = stof(*i) ;
        }
        if (stof(*i)  < min){
            min = stof(*i) ;
        }
            avg += stof(*i);
        i++;
    }
    avg /= _data.size();
    cout << "2.Avg/Max/Min temperature: " << avg << "/" << max << "/"  << min << endl;
}

ViewDataThree::ViewDataThree(){}

void ViewDataThree::print(string _temperature, string _humidity, string _pressure, list <string> _data){
    float humidity = 0;
    humidity = stof(_humidity);
    if (humidity > 0 && humidity <= humLow){
        cout << "3.Low probability of rain" << endl;
    }
    else if (humidity > humLow && humidity <= humMedium){
        cout << "3.There is a chance of rain" << endl;
    }
    else if (humidity > humMedium && humidity <= humHigh){
        cout << "3.High probability of rain" << endl;
    }
}

void Parsero(string _data, string _dataVec[]){
    stringstream DataStream(_data);  
    getline(DataStream, _dataVec[0], ',');
    getline(DataStream, _dataVec[1], ',');
    getline(DataStream, _dataVec[2], ',');
}

int main(){
    list <string> temp;
    char key;
    string data, _data[nDatos];
    ViewData *ptr[] = {new ViewDataOne(), new ViewDataTwo(), new ViewDataThree()};
    do{
    cout << "Insert (T[C],H[%],P[kPa]): ";
    cin >> data;
    Parsero(data, _data);
    temp.push_back(_data[0]);
    for (uint8_t i = 0; i < sizeof(ptr)/sizeof(ptr[0]) ; i++)
    {
        ptr[i]->print(_data[0], _data[1], _data[2], temp);
    }
    cout << "Desea agregar mas datos ? (s/n)" << endl;
    cin >> key;
    }while(key == 's' || key == 'S');
    return 0;
}
